#!/usr/bin/python3
##
## prep-eu.py
##
## Prepare EU-related maps. Merge data.
## Output is a JavaScript definition of
## the geojson data.
##
## output var is named `dr_data_eu`.
## Relevant feature attributes:
##  `DESCR` -> description text
##  `STATUS` -> data retention status
##    (0 = no DR, 1 = no DR, but had some
##     2 = currently disabled, 3 = DR avtive)
##
## (c) 2022 ulif <uli@gnufix.de>
##
## License: gpl-3.0-or-later
##
##############################################
import csv
import json
import os


GEODATA_JSON = "eu-countries.geojson"
DRDATA_CSV = "dr-in-eu.csv"
OUTFILE = "dataretention_eu.js"


def merge_data(geodata_json, drdata_csv):
    with open(drdata_csv) as csvfile:
        data = dict([(row["CNTR_ID"], row)
            for row in csv.DictReader(csvfile, delimiter=";")])
    geodata = json.load(open(geodata_json))
    for feature in geodata["features"]:
        cntr_id = feature["properties"]["CNTR_ID"]
        if cntr_id not in data.keys():
            print("WARNING! No data for: %s" % cntr_id)
            continue
        feature["properties"]["DESCR"] = data[cntr_id]["DESCR_DE"]
        feature["properties"]["STATUS"] = data[cntr_id]["STATUS"]
    return geodata


def geojson2js(data, outfile):
    with open(outfile, "w") as fd:
        fd.write("var dr_data_eu = [%s];" % json.dumps(data))
    print("Result written to ", outfile)


geojson2js(merge_data(GEODATA_JSON, DRDATA_CSV), OUTFILE)

