# Map: Data Retention in the EU

Date of Data: 2019

This map displays state of data retention laws in different EU countries. It is
based on a map provided 2019 by https://netzpolitik.org but with updates
applied to reflect current state of data retention in Europe.

The map data is contained in `dataretention_eu.js`, which is compiled with data
from `dr-in-eu.js` (status of dataretention in EU countries) and
`eu-countries.geojson`.

You can generate the `dataretention_eu.js` by running the local `prep-eu.py`
script.


Sources:

- Initial infos:
[0]( (2019): https://netzpolitik.org/2019/vorratsdatenspeicherung-in-europa-wo-sie-in-kraft-ist-und-was-die-eu-plant/)

* https://umap.openstreetmap.fr/de/map/die-vorratsdatenspeicherung-in-der-eu_333166?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=false&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=undefined&captionBar=false#5/54.674/10.085

- Geo-Data: Based on "Countries, 2020 - Administrative Units - Dataset" of the
  European Commission.
  Source: https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/administrative-units-statistical-units/countries
  License/Terms of Use/Attribution: Generalised dataset derived from
  EuroGeographics, Turkstat and UN-FAO GI data. The dataset may be used and
  distributed if: The source (EuroGeographics and UN-FAO) is acknowledged, AND
  The data is not used for commercial purpose, AND The original geometry is
  generalised to the equivalent of a scale of 1:1.000.000 or smaller. The
  source, copyright and branding will be acknowledged if the geographic data
  are used in Commission products. The acknowledgement will be displayed as
  “@EuroGeographics” on the map or in an acknowledgement text. The size of the
  text on the map will be proportional to the size of the map. The maximum
  length of copyright texts on electronic maps (web maps or electronic
  applications) is 20 characters. No copyright text will be applied for online
  icon maps with less than 150 x 150 pixels.
