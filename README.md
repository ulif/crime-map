![Banner: Europe is watching you](https://ulif.codeberg.page/crime-map/wp-plugin/banner-772x250.png)

# crime-map

Interactive maps of crime and data retention in Europe.

Contains also a Wordpress plugin for easier deployment.

Current status: v0.6 contains maps of Germany and Belgium.

(c) 2022 ulif <uli@gnufix.de>
License: GPL-3.0-or-later

This project was partially funded by Patrick Breyer MEP (Greens/EFA group,
Pirate Party).


![screenshot: Map of Belgium nearly all red](https://ulif.codeberg.page/crime-map/screenshot1.jpg)


## 0 Quick install

### Locally, static webserver

Local `site-content` is a directory with static content you can serve with your
webserver. What is missing, is a copy of `leaflet`, which has to be put in place:

Unpack current version of `leaflet` in `site-content/leaflet`. Get it from

    https://leafletjs.com/download.html

and unpack the .zip file in `site-content/leaflet/`:

    $ unzip -d site-content/leaflet leaflet.zip

Ready. Put the site-content/ dir wherever the webserver can find it and enjoy.

For a local test try

    $ python3 -m http.server --directory site-content 2323

and browse the map at http://localhost:2323 (German map) or http://localhost:2323/index-be.html (Belgian map).


### Wordpress

A ready-to-use Wordpress plugin can be obtained from

  https://ulif.codeberg.page/wp-plugin/crime-map.zip

Unpack it in your `plugins` folder, activate, enjoy.

The wordpress plugin simply defines shortcodes to display the maps in pages or posts. Currently you can use

  `[crimemap]`

to create a map of possible data retention in Germany or

  `[crimemap_be]`

for a Belgian map.


## 1 What is this? Layers of data retention coming...

After plenty judgements of courts in nearly all European countries that declared
data retention illegal, the countries are still eager to reintroduce this
zombie of net politics.

The newest idea is, to limit data retention somehow to some times or areas, so that
on next trial the countries can tell the judges, that they do not monitor all of
the population all of the time, but only 2/3 of it.

Additionally, they started to think of several layers of data retention happening
at the same time. That means: while they monitor parts of the country, because
of an above average crime rate, they might monitor other places and persons for different
reasons. Alltogether, they might scan the whole population, but for different reasons.

In Belgium, for instance, they government plans to introduce *5 Layers of data retention*
at the same time.


## 2 What do the maps represent?

### 2.1 Germany

The German map provided here shows areas of Germany would be under data retention
when a municipalitiy has an above average crime rate.


### 2.2 Belgium

The Belgian map also shows areas (in red), for which the government thinks, that
crime has "taken root" in. Contrary to Germany, the Belgian plan sets a fixed
threshold, which is *not* an average value of anything (the nationwide crime
rate for instance).

Instead the Belgian government simply decided, that 3 serious crimes per 1.000
population mark a dangerous area, where crime has taken roots, so that data
retention should happen right there.

It is yet a bit unclear, what a "crime" with respect to the Belgian government
plans means. The draft lists offences of about 45 different Belgian laws, that
might be relevant.

Our investigations showed that the threshold, apparently arbitrarily set by
the government, marks the whole country as a zone of serious crime and makes
it a target of extensive data retention.

The map marks only one of 5 layers of data retention, the government is willing to
activate in Belgium.


## 3 Making a map 1: How we freed the data

For Germany the computation was relatively easy. Official sources exist for
population, geo data and crime rates. The crime rates are slightly hidden by the
federal police (BKA) in spreadsheets, but all data is more or less machine-
readable and available under Open Data licenses.

In Belgium things are more difficult. Apparently the police in Belgium publishes
basic crime data partly on fancy internet pages with funny forms which on partly
work.

We needed more accurate data, because gross numbers of overall crime can not
be compared to the above mentioned list of offences listed in more than 45
different laws.

Luckily, the Belgian federal police publishes pretty detailed data about sorts
of crime and the places where it happened on their website. The bad news is:
the data is scattered about hundreds of PDF files and PDF is a format nearly
unreadable for machines.

### Step 1: Download many hundreds of PDF reports

We therefore hacked some tools. Step 1: We fetched all available data
(581 reports for each municipality/town, 185 reports/files for all the
police zones in Belgium and finally 13 district reports, showing crime data
for the judicial districts of Belgium. All these reports are available in french
and dutch.

Trying to download all these files we noticed, that for the towns of a few police
zones the respective reports are missing (404 - not found) - at least the
french versions. The respective dutch versions could all be found.


### Step 2: Scan PDFs to extract the crime numbers

As regular PDF scanners were completely overcharged with the tables showing up
in the reports, we programmed our own one. We learned, that a few reports were
apparently manually modified (breaking the otherwise consistent table layout) and
discovered, that all dutch reports on municipality level lack data about certain
sorts of crime (which are contained in the respective french reports).

We therefore only considered the reports on police zone level and judicial
province (arrondissement judicaire) level.

As a result from the scanning we got a complete list of offences (sorts of
crime), listed in at least one police zone or province report.

We got over 1.000 different kinds of crime.


### Step 3: Determine which offences could trigger data retention

We then determined, which sorts of crime could be meant by "art. 90ter" of the
code of criminal procedure (the one mentioned above listing dozens of other laws).

We filtered the collected data this way and got the crime rates, we were interested
in, in machine-readable format.


### Step 4: Join all together

The geo data was retrieved from https://geo.be, the geo data portal of Belgian
federal institutions and then manually treated (with QGIS) to show the areas we
collected data for.

The population data was retrieved from the federal Belgian statistics office
"statbel". The provided spreadsheets could easily be handled.

Some more scripts to put all this together resulted in the data we present in the
Belgian map.

All scripts will be provided here, so you can reproduce each single step.

We are happy to send all data we digged in a format you and your machines like. We
found JSON to be of great help during processing  of all the data.


## Sources:

### German Map

Current data based on 2020 criminality report of Bundeskriminalamt (German
federal crime investigation office).

KR-F-01-T01-Kreise-Faelle-HZ\_csv.csv (c) 2020 Bundeskriminalamt
License: dl-de/by-2-0  (https://www.govdata.de/dl-de/by-2-0)

vg5000\_KRS.shp (c) 2022 GeoBasis-DE / BKG
License: dl-de/by-2-0  (https://www.govdata.de/dl-de/by-2-0)


### Belgian Map

Crime Data: Belgian Federal Police:
https://www.stat.policefederale.be/statistiquescriminalite/rapports/

Geo Data: Geo.be
https://www.geo.be/catalog/details/591e7f88-c443-4659-b8b7-23601d647ee6?l=en

Population Data:

- https://statbel.fgov.be/sites/default/files/files/opendata/bevolking%20naar%20woonplaats%2C%20nationaliteit%20burgelijke%20staat%20%2C%20leeftijd%20en%20geslacht/TF\_SOC\_POP\_STRUCT\_2020.zip

- https://statbel.fgov.be/sites/default/files/files/opendata/bevolking%20naar%20woonplaats%2C%20nationaliteit%20burgelijke%20staat%20%2C%20leeftijd%20en%20geslacht/TF\_SOC\_POP\_STRUCT\_2020.xlsx


