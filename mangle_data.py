#!/usr/bin/python3
# mangle_data.py
#
# (c) 2022 ulif <uli@gnufix.de>
# GPL-3.0-or-later
#
# Extract data from Kreiskriminalstatistik
# Creates geojson data with data from Kriminalstatistik added.
# We produce 2 files:  a .geojson file which can be read from JavaScript
# and a .js file which has to be read from HTML. You will only need one of
# both.
#
# As input we need two or three file:
# 1) The file with the crime data
# 2) The file with the geo-data (where the crimes are located)
# 3) (optionally) a file containing mapping of geo location to population data
# if the population data is included in the crime data file, 3) is not needed.
#
import csv
import json
import re


#CRIMFILE = "KR-F-01-T01-Kreise-Faelle-HZ_csv.csv"
#CRIMEFILE_ENCODING = "iso-8859-1"
#CRIME_KEY = "Straftaten insgesamt"
#CRIME_LOC_KEY = "3"  # column containing Gemeindeschluessel
#GEOFILE = "vg5000.geojson"
#GEOFILE_LOC_KEY = "AGS"  # attribute containing Gemeindeschluessel
#GEOFILE_LOC_NAME = "GEN" # attribute containing place name
#RESULTFILE = "crimedata.geojson"
#CRIME_KEY_COL = "1"
#CRIME_NAME_COL = "2"
#CRIME_DATA_MAP = {
#    "cases": "6",
#    "tries": "8",
#    "freq_number": "7",
#    "suspects": "14",
#    }
#CRIME_DATA_PER_LOC = dict(
#    CRIM_CASES="cases",
##    CRIM_TRIES="tries",
#    CRIM_CASES_PER_100K="freq_number",
#    )
#POPULATION_FILE = "population-kreise-2020.csv"
#POPULATION_COL_LOC = "location"    # column containing location code
#POPULATION_COL_POP = "population"  # column containung population numbers


#CRIMFILE = "LA-F-06-T02-Laender-Faelle-Wikri-HZ_csv.csv"
#CRIMEFILE_ENCODING = "iso-8859-1"
#CRIME_KEY = "Straftaten insgesamt"
#CRIME_LOC_KEY = "3"  # column containing Gemeindeschluessel
#GEOFILE = "vg5000-lan.geojson"
#GEOFILE_LOC_KEY = "GEN"  # attribute containing Gemeindeschluessel
#GEOFILE_LOC_NAME = "GEN" # attribute containing place name
#RESULTFILE = "crimedata_lan.geojson"
#CRIME_KEY_COL = "1"
#CRIME_NAME_COL = "2"
#CRIME_DATA_MAP = {
#    "cases": "4",
#    # "tries": "6",
#    "freq_number": "5",
#    # "suspects": "10",
#    }
#CRIME_DATA_PER_LOC = dict(
#    CRIM_CASES="cases",
#    # CRIM_TRIES="tries",
#    CRIM_CASES_PER_100K="freq_number",
#    )
#POPULATION_FILE = "population-laender-2020.csv"
#POPULATION_COL_LOC = "location"    # column containing location code
#POPULATION_COL_POP = "population"  # column containung population numbers


CRIMFILE = "straftaten-tm-internet-2020.csv"
CRIMEFILE_ENCODING = "utf-8"
CRIME_KEY = "Straftaten insgesamt"
CRIME_LOC_KEY = "4"  # column containing Gemeindeschluessel
GEOFILE = "vg5000-lan.geojson"
GEOFILE_LOC_KEY = "GEN"  # attribute containing Gemeindeschluessel
GEOFILE_LOC_NAME = "GEN" # attribute containing place name
RESULTFILE = "crimedata_tmi.geojson"
CRIME_KEY_COL = "2"
CRIME_NAME_COL = "3"
CRIME_DATA_MAP = {
    "cases": "5",
    # "tries": "6",
    "freq_number": "HZ",
    # "suspects": "10",
    }
CRIME_DATA_PER_LOC = dict(
    CRIM_CASES="cases",
    # CRIM_TRIES="tries",
    CRIM_CASES_PER_100K="freq_number",
    )
POPULATION_FILE = "population-laender-2020.csv"
POPULATION_COL_LOC = "location"    # column containing location code
POPULATION_COL_POP = "population"  # column containung population numbers


# this is, what according to BKA, an integer number looks like...
# matches "123,456", "1,234", "0", "2,123,456", etc.
RE_BKA_INTEGER = re.compile("^[0-9]+(,[0-9]{3})*$")
RE_BKA_FLOAT = re.compile("^[0-9]+\.[0-9]+$")
FILTER_OUT_LOCS = ["Bundesrepublik Deutschland"]

def read_population_file(src_path):
    """Read population data from CSV file.

    Returns dict with locations as keys and populations as values.
    Expects colsumns called  "location" and "population" in file.
    """
    csv_data = dict()
    with open(src_path, "r") as csv_file:
        reader = csv.DictReader(csv_file, delimiter=";")
        # next(reader)  # skip header row
        for row in reader:
            if len(row[POPULATION_COL_LOC]) == 0:
                continue
            if len(row[POPULATION_COL_POP]) == 0:
                continue
            loc_code = row[POPULATION_COL_LOC]
            if RE_BKA_INTEGER.match(loc_code):
                loc_code = int(loc_code)
            csv_data[loc_code] = int(row[POPULATION_COL_POP])
    return csv_data


def read_csv_data(src_path, encoding, location_key, pop_data):
    csv_data = dict()
    with open(src_path, "r", encoding=encoding) as csv_file:
        reader = csv.DictReader(csv_file, delimiter=";")
        cnt = 0
        for row in reader:
            if row[location_key] in FILTER_OUT_LOCS:
                continue
            crime_key = row[CRIME_NAME_COL]
            if crime_key != "Straftaten insgesamt":
                crime_key = "%s (%s)" % (crime_key, row[CRIME_KEY_COL])
            if cnt == 0:
                cnt += 1
                continue
            # remove "," from numbers...
            for key in row.keys():
                if RE_BKA_INTEGER.match(row[key]):
                    row[key] = int(row[key].replace(",", ""))
                elif RE_BKA_FLOAT.match(row[key]):
                    row[key] = float(row[key])
            if crime_key not in csv_data.keys():
                csv_data[crime_key] = dict()
            # ensure, we do not overwrite data
            assert row[location_key] not in csv_data[crime_key].keys()
            assert row[location_key] in pop_data.keys()
            data_set = [
                (key, row[val])
                for key, val in CRIME_DATA_MAP.items()
            ]
            data_set.append(("popl", pop_data[row[location_key]]), )
            csv_data[crime_key][row[location_key]] = dict(data_set)
    return csv_data


def get_additional_data(csv_data, places, total, per100k):
    """Compute more overall datapoints to be added in results
    """
    # how many inhabtants live in places with more
    # than overall number of cases/100k?
    pop_cases_over_avg = sum(
        [
            csv_data[x]["popl"]
            for x in places
            if csv_data[x]["freq_number"] >= per100k["cases"]
        ]
    )
    print("AVG: ", per100k["cases"], pop_cases_over_avg, total["popl"])
    print(
        [
            csv_data[x]["popl"]
            for x in places
            if csv_data[x]["freq_number"] >= per100k["cases"]
        ]
    )
    # percentage of population living in such places
    pop_percent_cases_over_avg = pop_cases_over_avg * 100.0 / total["popl"]
    print(" Add aggregated var: POP_PERCENT_CASES_OVER_AVG")
    return dict(
        POP_PERCENT_CASES_OVER_AVG=pop_percent_cases_over_avg)


def create_resultfile(csv_data, crime_key=CRIME_KEY, suffix=""):
    places = list(csv_data.keys())
    total, average, maxvals, per100k = dict(), dict(), dict(), dict()
    # compute max, average, and total of all interesting cols
    print(csv_data)
    print([csv_data[x]['popl'] for x in places])
    for col in list(CRIME_DATA_MAP.keys()) + ["popl"]:
        total[col] = sum([csv_data[x][col] for x in places])
        average[col] = total[col] / len(places)
        maxvals[col] = max([csv_data[x][col] for x in places])
    # if "popl" in CRIME_DATA_MAP.keys():
    # compute values per 100k poulation for all keys
    for col in CRIME_DATA_MAP.keys():
        per100k[col] = total[col] / total["popl"] * 100000
    additional_data = get_additional_data(csv_data, places, total, per100k)
    with open(GEOFILE, "r") as geo_file:
        geo_data = json.load(geo_file)
    agg = dict(total=total, average=average, maxvals=maxvals, per100k=per100k)
    # add global (overall) data
    for cat, data in agg.items():
        for key in data.keys():
            var_name = "CRIM_%s_%s" % (cat.upper(), key.upper())
            print(" Add aggregated var: %s" % var_name)
            geo_data[var_name]=data[key]
    geo_data.update(additional_data.items())
    print(" Add aggregated var: CRIMETYPE")
    geo_data["CRIMETYPE"] = crime_key
    # add data for each location...
    for num, elem in enumerate(geo_data["features"]):
        loc_key = elem["properties"][GEOFILE_LOC_KEY]
        if RE_BKA_INTEGER.match(loc_key):
            loc_key = int(loc_key)
        if loc_key not in csv_data.keys():
            print("WARNING: no crime data found for '%s'." % loc_key)
            print("  This location will provide no crime data")
            continue
        # ensure, we do not overwrite data
        assert "CRIM_CASES" not in elem["properties"].keys()
        criminal_data = csv_data[loc_key]
        elem["properties"] = dict(
            NAME=elem["properties"][GEOFILE_LOC_NAME])
        for var_name, key in CRIME_DATA_PER_LOC.items():
            if num == 1:
                print(" Add per-location: %s" % var_name)
            elem["properties"][var_name] = criminal_data[key]
    # writeout resultfiles
    filename = RESULTFILE
    if suffix:
        filename = suffix.join(filename.rsplit(".", 1))
    # write .geojson file
    with open(filename, "w") as result_file:
        json.dump(geo_data, result_file)
    print("result in %s ('%s')" % (filename, crime_key))
    # write .js file
    basename = filename.rsplit(".", 1)[0]
    filename_js = "%s.js" % basename
    basename = basename.rsplit("/", 1)[-1]
    with open(filename_js, "w") as result_file:
        result_file.write("var %s = [%s];" % (basename, json.dumps(geo_data)))
    print("result in %s ('%s')" % (filename_js, crime_key))


pop_data = read_population_file(POPULATION_FILE)
csv_data = read_csv_data(CRIMFILE, CRIMEFILE_ENCODING, CRIME_LOC_KEY, pop_data)

## Create output for each of the crime keys....
#for num, crime_key in enumerate(csv_data.keys()):
#    create_resultfile(csv_data[crime_key], crime_key, "_%02i." % num)

## or... create the basic and main data
create_resultfile(csv_data[CRIME_KEY], CRIME_KEY)
