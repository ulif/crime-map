#!/usr/bin/python3
#
# Turn geojson files into pure javascript
#
# which means: prepend "var varname=[" before and put "];" at end.
# We handle all .geojson files and create .js files.
#
# (c) 2022 ulif <ulif@gnufix.de>
# GPL-3.0-or-later
# 
import os


SRC_DIR = "."


for name in os.listdir():
    if not name.endswith(".geojson"):
        continue
    base = name.split(".", 1)[0]
    content = ""
    print("reading " + name)
    with open(name, "r") as fp:
        content = fp.read()
    content = "var %s = [%s];" % (base, content)
    print("writing " + base + ".js")
    with open(base + ".js", "w") as fp:
        fp.write(content)
print("done.")
