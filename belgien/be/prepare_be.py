#/usr/bin/python3
#
# Create a crimedata-be.js tp visualize crime rates in Belgium.
#
# (c) 2022 ulif <uli@gnufix.de>
# GPL-3.0-or-later
#
import csv
import json


CRIME_DATA = "rawcrimedata_be.json"
POP_DATA = "pop_be.csv"
PZ_DATA = "police_zone_with_pop.json"
GEO_DATA = "be_towns.geojson"
YEAR = 2020  # the year for which we collect data
RESULT_GEOFILE = "crimedata_be.js"


def read_pop_csv(path):
    pop_data = dict()
    with open(path) as fd:
        r = csv.DictReader(fd, delimiter=";")
        for row in r:
            pop_data[row["CD_REFNIS"]] = row
    return pop_data


def get_loc_ids(pop_data):
    regions, provinces, districts, towns = dict(), dict(), dict(), dict()
    for key, entry in pop_data.items():
        if entry["CD_DSTR_REFNIS"] not in districts.keys():
            districts[entry["CD_DSTR_REFNIS"]] = dict(
                descr_nl=entry["TX_ADM_DSTR_DESCR_NL"],
                descr_fr=entry["TX_ADM_DSTR_DESCR_FR"]
                )
        if entry["CD_PROV_REFNIS"] not in (provinces.keys(), ''):
            provinces[entry["CD_PROV_REFNIS"]] = dict(
                descr_nl=entry["TX_PROV_DESCR_NL"],
                descr_fr=entry["TX_PROV_DESCR_FR"]
                )
        if entry["CD_RGN_REFNIS"] not in regions.keys():
            regions[entry["CD_RGN_REFNIS"]] = dict(
                descr_nl=entry["TX_RGN_DESCR_NL"],
                descr_fr=entry["TX_RGN_DESCR_FR"]
                )
        if entry["CD_REFNIS"] not in towns.keys():
            towns[entry["CD_REFNIS"]] = dict(
                descr_nl = entry["TX_DESCR_NL"],
                descr_fr = entry["TX_DESCR_FR"],
                )
    return regions, provinces, districts, towns


CRIMEDATA_FIXES = {
    "Brabant wallon":  "Waals-Brabant",
    "Braine-l'Alleud": "Braine-l’Alleud",
    "Fontaine-l'Evêque": "Fontaine-l’Evêque",
    "Mont-de-l'Enclus": "Mont-de-l’Enclus",
    "Blégny": "Blegny",
    "Villers-le-Bouillet": "Villers-Le-Bouillet",
    }


def fix_crime_loc_ids(pop_data, raw_crime):
    """Find NIS id of towns etc.
    """
    result = []
    regs, provs, dstrs, towns = pop_data
    rdict = dict([(v["descr_nl"], k) for k, v in regs.items()])
    pdict = dict([(v["descr_nl"], k) for k, v in provs.items()])
    tdict1 = dict([(v["descr_nl"].split("(")[0].strip(), k) for k, v in towns.items()])
    tdict2 = dict([(v["descr_fr"].split("(")[0].strip(), k) for k, v in towns.items()])
    tdict = dict(list(tdict1.items()) + list(tdict2.items()))
    for entry in raw_crime:
        cname = CRIMEDATA_FIXES.get(entry[3], entry[3])
        if entry[0] < 10:
            continue
        if entry[2] == 1:
            result.append(["01000"] + entry)
            continue
        elif entry[2] == 2:
            rid = rdict.get(cname, None)
            if rid is not None:
                result.append([rid] + entry)
            else:
                print("cannot find %s in %s" % (
                    cname, rdict.keys()))
        elif entry[2] == 3:
            pid = pdict.get("Provincie " + cname, None)
            if pid is None:
                print("cannot find PROVINCE", cname)
        elif entry[2] == 6:
            tid = tdict.get(cname, None)
            if tid is None:
                print("cannot find TOWN: ", cname)
                continue
            result.append([tid] + entry)
    return result


def merge_pop_crime(loc_fixed_crime, pz_data, crime_index, pop_sum):
    crimes = dict((x[0], x) for x in loc_fixed_crime)
    nat_data = dict(
        alt_name="Belgique", name="België",
        nisid="01000", pop=pop_sum, pzones=pz_data,
        crimes=int(crimes["01000"][crime_index].replace(".", "")))
    for pz in nat_data["pzones"]: # ]pz_data:
        pz_crimes = 0
        for town in pz["municips"]:
            cdata = crimes[town["nisid"]]
            crime_cnt = int(cdata[crime_index].replace(".", ""))
            town["crimes"] = crime_cnt
            town["pop"] = int(town["pop"])
            pz_crimes += crime_cnt
        pz["crimes"] = pz_crimes
    return nat_data


def merge_geo_pop_crime(geo_data, crime_pop_data):
    crim_total_population = crime_pop_data["pop"]
    crim_total_cases=crime_pop_data["crimes"]
    crim_per100k_cases=100000 * crim_total_cases / crim_total_population
    ctowns = dict()
    for pz in crime_pop_data["pzones"]:
        for town in pz["municips"]:
            ctowns[town["nisid"]] = town
    pop_in_suspect_areas = 0
    for elem in geo_data["features"]:
        ctown = ctowns[elem["properties"]["AdMuKey"]]
        new_props = dict(
            NAME=ctown["name"],
            CRIM_CASES=ctown["crimes"],
            CRIM_CASES_PER_100K=int(100000 * ctown["crimes"] / ctown["pop"]))
        if new_props["CRIM_CASES_PER_100K"] > crim_per100k_cases:
            pop_in_suspect_areas += ctown["pop"]
        elem["properties"] = new_props
    pop_percent_cases_over_avg = 100 * pop_in_suspect_areas / crim_total_population
    geo_data.update(
        CRIM_TOTAL_POPULATION=crim_total_population,
        CRIM_TOTAL_CASES=crim_total_cases,
        CRIM_PER100K_CASES=crim_per100k_cases,
        POP_IN_SUSPECT_AREAS=pop_in_suspect_areas,
        POP_PERCENT_CASES_OVER_AVG=pop_percent_cases_over_avg,
        CRIMETYPE="Offences in 2020"
        )
    return geo_data


raw_crime = json.loads(open(CRIME_DATA).read().encode("utf-8"))["aaData"]
crime_year_index = raw_crime[0].index(2020) + 1
pop_data = read_pop_csv(POP_DATA)
pop_sum = int(pop_data["Summe Ergebnis"]["MS_POPULATION"])
del(pop_data["Summe Ergebnis"])
geo_data = json.loads(open(GEO_DATA).read().encode("utf-8"))
pz_data = json.loads(open(PZ_DATA).read().encode("utf-8"))
loc_data = get_loc_ids(pop_data)
loc_fixed_crime = fix_crime_loc_ids(loc_data, raw_crime)
crime_pop = merge_pop_crime(loc_fixed_crime, pz_data, crime_year_index, pop_sum)
final_geo = merge_geo_pop_crime(geo_data, crime_pop)
with open(RESULT_GEOFILE, "w") as fd:
    fd.write("var crimedata_be = [%s];" % json.dumps(final_geo))
print("Result written to ", RESULT_GEOFILE)

