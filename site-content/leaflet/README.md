# leaflet folder

If this folder is nearly empty, then a copy of the `leaflet` library is
missing. Get it from

    https://leafletjs.com/download.html

and unpack the .zip file in this folder:

    $ unzip -d site-content/leaflet leaflet.zip

That's it. Read the main README.md for more infos.
