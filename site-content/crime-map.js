document.addEventListener("DOMContentLoaded", function(event) {
    var mymap = L.map('mapid').setView([51.5, 10.5], 7);
    L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, crime data &copy; <a href="https://www.bka.de/DE/AktuelleInformationen/StatistikenLagebilder/PolizeilicheKriminalstatistik/PKS2020/PKSTabellen/KreisFalltabellen/kreisfalltabellen_node.html">PKS BKA, 2020 (V1.0)</a> <a href="https://www.govdata.de/dl-de/by-2-0">dl-de/by-2-0</a>, geo data: &copy; GeoBasis-DE / BKG 2022  <a href="https://www.govdata.de/dl-de/by-2-0">dl-de/by-2-0</a>' ,
    }).addTo(mymap);
    // at this point, `crimedata` must be defined (read from data/crimedata.js)
    var data = crimedata[0];
    var geojson;

    function highlightFeature(e) {
        var layer = e.target;

        layer.setStyle({
            weight: 2,
            color: '#ff9',
            dashArray: '',
            fillOpacity: 0.7
        });

        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }

        //info.update(layer.feature.properties);
    }

    function resetHighlight(e) {
        geojson.resetStyle(e.target);
        //info.update();
    }

    function zoomToFeature(e) {
        mymap.fitBounds(e.target.getBounds());
    }

    function onEachFeature(feature, layer) {
        var crime_rate = String(Math.round(feature.properties.CRIM_CASES_PER_100K)).replace(/(.)(?=(\d{3})+$)/g, '$1.');
        layer.on({
            mouseover: highlightFeature,
            mouseout: resetHighlight,
            click: zoomToFeature
        });
        layer.bindPopup( "<strong>" + feature.properties.NAME + "</strong>" +
            "<br/>" + String(feature.properties.CRIM_CASES).replace(/(.)(?=(\d{3})+$)/g, '$1.') + " criminal offences in 2020" +
            "<br />" + crime_rate + " per 100.000 population"
        );
    }

    function style(feature) {
        var fillColor,
            crime_rate = feature.properties.CRIM_CASES_PER_100K,
            crime_avg = data.CRIM_PER100K_CASES;
        if (crime_rate < crime_avg) fillColor = '#00ff00';
        else fillColor = '#ff0000';
        return {
            fillColor: fillColor,
            weight: 1,
            color: '#88888800',
            fillOpacity: 0.5,
            //fillColor: '#11111111',
            opacity: 0.99,
            //dashArray: '3',
        };
    }
 
    // add GeoJSON layer to the map once the file is loaded
    geojson = L.geoJson(data, {
        style: style,
        onEachFeature: onEachFeature
    }).addTo(mymap);

});
