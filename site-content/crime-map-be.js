document.addEventListener("DOMContentLoaded", function(event) {
    var mymap = L.map('mapidBE').setView([50.75, 4.5], 8);
    L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, crime data &copy; <a href="https://www.stat.policefederale.be/statistiquescriminalite/rapports/">DRI/BIPOL</a> , geo data: <a href="https://www.geo.be/catalog/details/591e7f88-c443-4659-b8b7-23601d647ee6?l=de">GAPD Apn_AdMu</a> &copy; <a  href="geo.be">geo.be </a> 2017, <a href="http://inspire.ec.europa.eu/metadata-codelist/LimitationsOnPublicAccess/noLimitations">Public Access</a>'
    }).addTo(mymap);
    // at this point, `crimedata` must be defined (read from data/crimedata.js)
    var data = crimedata_be[0];
    var geojson;

    function highlightFeature(e) {
        var layer = e.target;

        layer.setStyle({
            weight: 2,
            color: '#ff9',
            dashArray: '',
            fillOpacity: 0.7
        });

        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }

        //mapinfo.update(layer.feature.properties);
    }

    function resetHighlight(e) {
        geojson.resetStyle(e.target);
        //mapinfo.update();
    }

    function zoomToFeature(e) {
        mymap.fitBounds(e.target.getBounds());
    }

    function onEachFeature(feature, layer) {
        var crime_rate = (Math.round(feature.properties.CRIMES_PER_1K * 100)/100).toLocaleString();
        var avg_crime_rate = (Math.round(feature.properties.CRIMES_CNT * 100)/100).toLocaleString();
        var pop = feature.properties.POP.toLocaleString();
        layer.on({
            mouseover: highlightFeature,
            mouseout: resetHighlight,
            click: zoomToFeature
        });
        layer.bindPopup( "<strong>" + feature.properties.NAME + "</strong>" +
            "<br/>Population: " + pop +
            "<br/>On average " + avg_crime_rate + " offences per year <br/>from 2018 - 2020" +
            "<br/>" + crime_rate + " offences per " + (1000).toLocaleString() + " population"
        );
    }

    function style(feature) {
        var fillColor = '#dddd00',
            crime_rate = feature.properties.CRIMES_PER_1K;
        if (crime_rate > 3.0) fillColor = '#f40';
        if (crime_rate >= 5.0) fillColor = '#e10';
        if (crime_rate >= 7.0) fillColor = '#a00'; // #b00
        //if (crime_rate >= 7.0) fillColor = '#dd0000ff';
        return {
            fillColor: fillColor,
            weight: 1,
            color: '#88888800',
            fillOpacity: 0.5,
            opacity: 0.99,
            dashArray: '3',
        };
    }

    // add GeoJSON layer to the map once the file is loaded
    geojson = L.geoJson(data, {
        style: style,
        onEachFeature: onEachFeature
    }).addTo(mymap);


	// control that shows state info on hover
	var info = L.control();

	info.onAdd = function (map) {
		this._div = L.DomUtil.create('div', 'mapinfo');
		this.update();
		return this._div;
	};

	info.update = function (props) {
		this._div.innerHTML = '<h1>Data Retention in Belgium</h1>' +  (props ?
			'<b>' + props.name + '</b><br />' + props.density +
            ' people / mi<sup>2</sup>' :
            'Average number of criminal offences against article 90ter <br> §§ 2 to 4 of Belgian criminal code of criminal procedure<br> per year from 2018 to 2020 for all police zones in Belgium.<br><br>This is layer 2 of the planned 5 layer data retention in Belgium.<br>' +
            'It contains juridicial districts, where from 2018 to 2020 more than<br>' +
            '3 offences per 1.000 inhabitants and per year happend.<br><br>' +
            '<b>This threshold is fixed and set by the government.<br><br>');
	};
	//info.addTo(mymap);


    // the legend in the lower right
	var legend = L.control({position: 'bottomright'});
	legend.onAdd = function (map) {
		var div = L.DomUtil.create('div', 'mapinfo maplegend');
        div.innerHTML += '<center>Months of Data<br>Retention/<br>Offences per<br>1.000 Inhabitants</center><br>'
        div.innerHTML += '<b style="background:#dddd00"></b> less than 3.0<br>' +
                         '<b style="background:#ff4400;">6</b> 3,0 &ndash; 4,99<br>' +
                         '<b style="background:#ee1100;">9</b> 5,0 &ndash; 6,99<br>' +
                         '<b style="background:#aa0000ff;">12</b> 7,0 or more<br>';
        return div;
	};

	legend.addTo(mymap);
});
