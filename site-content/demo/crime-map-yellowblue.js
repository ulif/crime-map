document.addEventListener("DOMContentLoaded", function(event) {
    var mymap = L.map('mapid-yellowblue').setView([51.5, 10.5], 7);
    L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, crime data &copy; <a href="https://www.bka.de/DE/AktuelleInformationen/StatistikenLagebilder/PolizeilicheKriminalstatistik/PKS2020/PKSTabellen/KreisFalltabellen/kreisfalltabellen_node.html">PKS BKA, 2020 (V1.0)</a> <a href="https://www.govdata.de/dl-de/by-2-0">dl-de/by-2-0</a>, geo data: &copy; GeoBasis-DE / BKG 2022  <a href="https://www.govdata.de/dl-de/by-2-0">dl-de/by-2-0</a>' ,
    }).addTo(mymap);
    fetch("./data/crimedata.geojson").then(res => res.json()).then(data => {
        // add GeoJSON layer to the map once the file is loaded
        L.geoJson(data, {
            style: function(feature) {
                var fillColor,
                    crime_rate = feature.properties.CRIM_CASES_PER_100K,
                    crime_max = data.CRIM_MAXVALS_FREQ_NUMBER,
                    // threshold: 
                    crime_avg = data.CRIM_AVERAGE_FREQ_NUMBER;
                var factor = crime_rate * 128.0 / crime_max;
                factor = parseInt(factor + 128).toString(16);
                if (crime_rate < crime_avg) fillColor = '#ffcc00';
                else fillColor = '#003399';
                return {
                    fillColor: fillColor,
                    weight: 2,
                    color: '#111',
                    // fillOpacity: 0.4,
                    fillOpacity: 0.2 + crime_rate / crime_max,
                    opacity: 0.99,
                }
            },
            onEachFeature: function( feature, layer ){
                var crime_rate = feature.properties.CRIM_CASES_PER_100K;
                layer.bindPopup( "<strong>" + feature.properties.NAME + "</strong>" +
                    "<br/>" + feature.properties.CRIM_CASES + " Straftaten (inkl. versuchte)" +
                    "<br />" + crime_rate + " pro 100.000 Einwohner.innen"
                )
            }
        }).addTo(mymap);
    });
});

