<?php
/**
 * Plugin Name: CrimeMap
 * Plugin URI! https://codeberg.org/ulif/crime-map
 * Description: Display maps of crime and data retention in Europe
 * Author: ulif
 * Author-URI: https://codeberg.org/ulif
 * License: GPL v3 or later
 * License-URI: https://www.gnu.org/licenses/gpl-3.0.html
 * Version: 0.6
 */

defined( 'ABSPATH' ) || exit;

add_action( 'init', 'crimemap_register_shortcodes' );

// register shortcodes
function crimemap_register_shortcodes() {
    add_shortcode( 'crimemap', 'crimemap_render_map' );
    add_shortcode( 'crimemap_be', 'crimemap_render_map_be' );
}

// shortcode callback
function crimemap_render_map() {
    $html = '<div style="min-height: 90vh;" id="mapid" ></div>';
    return $html;
}

function crimemap_render_map_be() {
    $html = '<div style="min-height: 90vh;" id="mapidBE" ></div>';
    return $html;
}

// register styles
function crimemap_register_styles() {
    wp_register_style(
        'leaflet-css',
        plugin_dir_url( __FILE__ ) . 'leaflet/leaflet.css',
        array(),
        '1.7.1',
        false
    );
    wp_register_style(
        'crimemap-css',
        plugin_dir_url( __FILE__ ) . 'crime-map.css',
        array(),
        '0.1',
        false
    );
}
add_action( 'init', 'crimemap_register_styles' );

// enqueue style - only if shortcode "crimemap" is present...
function crimemap_enqueue_styles() {
    if (( has_shortcode( get_the_content(), 'crimemap' ))
        || ( has_shortcode( get_the_content(), 'crimemap_be' ))) {
        wp_enqueue_style( 'leaflet-css' );
        wp_enqueue_style( 'crimemap-css' );
    }
}
add_action( 'wp_enqueue_scripts', 'crimemap_enqueue_styles');

// register scripts
function crimemap_register_scripts() {
    wp_register_script(
        'leaflet-js',
        plugin_dir_url( __FILE__ ) . 'leaflet/leaflet.js',
        array(),
        '1.7.1',
        false
    );
    wp_register_script(
        'crimemap1',
        plugin_dir_url( __FILE__ ) . 'crime-map.js',
        array( 'leaflet-js' ),
        '0.1',
        false
    );
    wp_register_script(
        'crimemap_data',
        plugin_dir_url( __FILE__ ) . 'data/crimedata.js',
        array( 'leaflet-js' ),
        '0.1',
        false
    );
    wp_register_script(
        'crimemapBE',
        plugin_dir_url( __FILE__ ) . 'crime-map-be.js',
        array( 'leaflet-js' ),
        '0.1',
        false
    );
    wp_register_script(
        'crimemap_data_be',
        plugin_dir_url( __FILE__ ) . 'data/crimedata_be.js',
        array( 'leaflet-js' ),
        '0.1',
        false
    );
}
add_action( 'init', 'crimemap_register_scripts' );

// enqueue scripts
function crimemap_enqueue_scripts() {
    if ( has_shortcode( get_the_content(), 'crimemap' )) {
        wp_enqueue_script( 'leaflet-js' );
        wp_enqueue_script( 'crimemap1' );
        wp_enqueue_script( 'crimemap_data' );
    }
    if ( has_shortcode( get_the_content(), 'crimemap_be' )) {
        wp_enqueue_script( 'leaflet-js' );
        wp_enqueue_script( 'crimemapBE' );
        wp_enqueue_script( 'crimemap_data_be' );
    }
}
add_action( 'wp_enqueue_scripts', 'crimemap_enqueue_scripts');


// automatic updates

if( ! class_exists( 'crimeMap' ) ) {

    class crimeMap{

        public $plugin_slug;
        public $version;
        public $cache_key;
        public $cache_allowed;

        public function __construct() {

            $this->plugin_slug = plugin_basename( __DIR__ );
            $this->version = '0.6';
            $this->cache_key = 'ulif_custom_upd';
            $this->cache_allowed = false;

            add_filter( 'plugins_api', array( $this, 'info' ), 20, 3 );
            add_filter( 'site_transient_update_plugins', array( $this, 'update' ) );
            add_action( 'upgrader_process_complete', array( $this, 'purge' ), 10, 2 );

        }

        public function request(){

            $remote = get_transient( $this->cache_key );

            if( false === $remote || ! $this->cache_allowed ) {

                $remote = wp_remote_get(
                    'https://ulif.codeberg.page/crime-map/wp-plugin/info.json',
                    //'http://pu.fritz.box:2424/wp-plugin/info.json',
                    array(
                        'timeout' => 10,
                        'headers' => array(
                            'Accept' => 'application/json'
                        )
                    )
                );

                if(
                    is_wp_error( $remote )
                    || 200 !== wp_remote_retrieve_response_code( $remote )
                    || empty( wp_remote_retrieve_body( $remote ) )
                ) {
                    return false;
                }

                set_transient( $this->cache_key, $remote, DAY_IN_SECONDS );

            }

            $remote = json_decode( wp_remote_retrieve_body( $remote ) );

            return $remote;

        }


        function info( $res, $action, $args ) {

            // print_r( $action );
            // print_r( $args );

            // do nothing if you're not getting plugin information right now
            if( 'plugin_information' !== $action ) {
                return $res;
            }

            // do nothing if it is not our plugin
            if( $this->plugin_slug !== $args->slug ) {
                return $res;
            }

            // get updates
            $remote = $this->request();

            if( ! $remote ) {
                return $res;
            }

            $res = new stdClass();

            $res->name = $remote->name;
            $res->slug = $remote->slug;
            $res->version = $remote->version;
            $res->tested = $remote->tested;
            $res->requires = $remote->requires;
            $res->author = $remote->author;
            $res->author_profile = $remote->author_profile;
            $res->download_link = $remote->download_url;
            $res->trunk = $remote->download_url;
            $res->requires_php = $remote->requires_php;
            $res->last_updated = $remote->last_updated;

            $res->sections = array(
                'description' => $remote->sections->description,
                'installation' => $remote->sections->installation,
                'changelog' => $remote->sections->changelog
            );

            if( ! empty( $remote->banners ) ) {
                $res->banners = array(
                    'low' => $remote->banners->low,
                    'high' => $remote->banners->high
                );
            }

            return $res;

        }

        public function update( $transient ) {

            if ( empty($transient->checked ) ) {
                return $transient;
            }

            $remote = $this->request();

            if(
                $remote
                && version_compare( $this->version, $remote->version, '<' )
                && version_compare( $remote->requires, get_bloginfo( 'version' ), '<=' )
                && version_compare( $remote->requires_php, PHP_VERSION, '<' )
            ) {
                $res = new stdClass();
                $res->slug = $this->plugin_slug;
                $res->plugin = plugin_basename( __FILE__ ); // crimemap/crimemap.php
                $res->new_version = $remote->version;
                $res->tested = $remote->tested;
                $res->package = $remote->download_url;

                $transient->response[ $res->plugin ] = $res;

            }

            return $transient;

        }

        public function purge( $upgrader, $options ){

            if (
                $this->cache_allowed
                && 'update' === $options['action']
                && 'plugin' === $options[ 'type' ]
            ) {
                // just clean the cache when new plugin version is installed
                delete_transient( $this->cache_key );
            }

        }


    }

    new crimeMap();

}

