function getColor(d) {
	return d > 98 ? '#ffff00cc' :
		d > 95   ? '#00ff00' :
		d > 90   ? '#00cc88' :
		d > 80   ? '#008888' :
		d > 50   ? '#048' : '#004';
}

document.addEventListener("DOMContentLoaded", function(event) {
	var mymap = L.map('mapid').setView([51.5, 10.5], 7);
	L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
		attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, crime data &copy; <a href="https://www.bka.de/DE/AktuelleInformationen/StatistikenLagebilder/PolizeilicheKriminalstatistik/PKS2020/PKSTabellen/KreisFalltabellen/kreisfalltabellen_node.html">PKS BKA, 2020 (V1.0)</a> <a href="https://www.govdata.de/dl-de/by-2-0">dl-de/by-2-0</a>, geo data: &copy; GeoBasis-DE / BKG 2022  <a href="https://www.govdata.de/dl-de/by-2-0">dl-de/by-2-0</a>' ,
	}).addTo(mymap);
	fetch("./data/crimedata-suspects.geojson").then(res => res.json()).then(data => {
		// add GeoJSON layer to the map once the file is loaded
		L.geoJson(data, {
			style: function(feature) {
				var fillColor,
					suspects = feature.properties.CRIM_SUSPECTS,
					pop = feature.properties.POPULATION,
					nice = 100 - (suspects * 100 / pop),
					crime_rate = feature.properties.CRIM_CASES_PER_HUNDREDK,
					crime_max = data.CRIM_MAX_CPHK,
					// threshold: avg. HZ (CPHK) or cases per 100.000 pop.
					// crime_avg = data.CRIM_AVG_CPHK;
					crime_avg = data.CRIM_AVG_CASES * 100000 / data.CRIM_AVG_POP;
				//fillColor = '#00ff00';
				fillColor = getColor(nice);
				//fillOpacity = (nice - 90) / 10.0;
				fillOpacity = 1.0;
				//if (nice > 98)
				//    fillColor = "#ff0";
				return {
					fillColor: fillColor,
					weight: 1,
					color: '#111',
					fillOpacity: 0.5,
					//fillOpacity: fillOpacity,
					opacity: 0.99,
				}
			},
			onEachFeature: function( feature, layer ){
				var suspects = feature.properties.CRIM_SUSPECTS,
					pop = feature.properties.POPULATION,
					percent = parseInt((suspects * 100 / pop) * 10) / 10;
				layer.bindPopup( "<strong>" + feature.properties.NAME + "</strong>" +
					"<br />Hier leben " + String(feature.properties.POPULATION).replace(/(.)(?=(\d{3})+$)/g, '$1.') + " Menschen" +
					"<br />von denen " + String(percent).replace(".", ",") + "% einer Straftat in 2020 tatverdächtig waren." +
					"<br /><b>" + String(100 - percent).replace(".", ",") + "% waren rechtstreu.</b>"
				)
			}
		}).addTo(mymap);
	});

	// control that shows state info on hover
	var info = L.control();

	info.onAdd = function (map) {
		this._div = L.DomUtil.create('div', 'info');
		this.update();
		return this._div;
	};

	info.update = function (props) {
		this._div.innerHTML = '<h1>97% rechtstreu</h1>' +  (props ?
			'<b>' + props.name + '</b><br />' + props.density +
			' people / mi<sup>2</sup>' : 'Über 97,3% der Menschen in Deutschland haben sich' +
			' 2020 <br /> nichts zuschulden kommen lassen.<br /><br />' +
			'Von der anlasslosen Vorratsdatenspeicherung wären <br />aber auch ' +
			'sie betroffen.<br /><br />' +
			'<h1>100% verdächtig</h1>');
	};

	info.addTo(mymap);



	var legend = L.control({position: 'bottomright'});

	legend.onAdd = function (map) {

		var div = L.DomUtil.create('div', 'info legend'),
			grades = [0, 50, 80, 90, 95, 98],
			labels = [];

		// loop through our density intervals and generate a label with a colored square for each interval
		for (var i = 0; i < grades.length; i++) {
			div.innerHTML +=
				'<i style="background:' + getColor(grades[i] + 1) + '"></i> ' +
				grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] + '<br>' : '+');
		}

		return div;
	};

	legend.addTo(mymap);
});

