#!/bin/bash
# Create WordPress plugin
# (c) 2022 ulif <uli@gnufix.de>
# GPL-v3-or-later
#
set -e
SRC_DIR=site-content
DST_DIR=tmp_create_wp_plugin/crimemap

# Collect sources
mkdir -p $DST_DIR/data
mkdir -p $DST_DIR/leaflet
for NAME in crimemap.php crime-map.css crime-map.js data/crimedata.js crime-map-be.js \
            data/crimedata_be.js; do
    echo "Copying $NAME"
    cp -ar $SRC_DIR/$NAME $DST_DIR/$NAME
done

echo "Downloading leaflet 1.7.1..."
wget -P $DST_DIR https://leafletjs-cdn.s3.amazonaws.com/content/leaflet/v1.7.1/leaflet.zip
unzip -d $DST_DIR/leaflet $DST_DIR/leaflet.zip
rm $DST_DIR/leaflet.zip

echo "Creating ZIP file..."
cd $DST_DIR/..
zip -r crime-map.zip *
cd -

echo "RESULT in $DST_DIR/../crime-map.zip"
